import axios from 'axios'
import { Message } from 'element-ui'
const instance = axios.create({
    baseURL: process.env.VUE_APP_baseURL
})
const http = (url, data = {}, method = "GET", params = {}, headers = {
    "Content-Type": "application/x-www-form-urlencoded"
}) => {
    return new Promise((resolve, reject) => {
        return instance({
            url,
            method,
            data,
            params,
            headers
        }).then(res => {
            if ((res.status >= 200 && res.status < 300) ||
                res.status === 304) {
                if (res.data.status === 0) {
                    console.log(res)
                    resolve(res.data)
                } else if (res.data.status === 10) {
                    router.replace({
                        path:'/login'
                    })
                }
                else {
                    Message({
                        message: res.data.msg,
                        type: "error"
                    })
                    console.log(res)
                    reject(res.data.msg)
                }
            } else {
                Message({
                    message: res.statusText,
                    type: "error",
                });
                reject(res.statusText)
            }
        }).catch(err => {
            console.log("error submit!!");
            return false;
        })
    })
}



export default http


