import App from './App.vue'
import router from './router'
import store from './store'
import 'reset-css'
import instance from './http/index'
import BreadCrumb from '@/components/BreadCrumb'
import Kong from '@/components/kong'
Vue.component('kong', Kong,)
Vue.component('bread-crumb', BreadCrumb,)
Vue.prototype.$axios = instance
Vue.config.productionTip = false


Vue.filter('formatNumber', function (value = '0', currencyType = '') {
  let res;
  if (value.toString().indexOf('.') === -1) {
    res = (value || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,')
  } else {
    let prev = value.toString().split('.')[0]
    let next = value.toString().split('.')[1] < 10 ? value.toString().split('.')[1] + '0' : value.toString().split('.')[1]
    res = (prev || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,') + '.' + next
  }
  return currencyType + res
});
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
