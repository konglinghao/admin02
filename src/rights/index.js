export default [{
        "authName": "仪表盘",
        "path": "first",
        "children": [{
            "authName": "首页",
            "path": "first",
        }],
    },
    {
        "authName": "商品",
        "path": "goods",
        "children": [{
                "authName": "商品管理",
                "path": "goods",
            },
        ],
    },
    {
        "authName": "品类",
        "path": "category",
        "children": [{
                "authName": "品类管理",
                "path": "category",
            },
        ],
    },
    {
        "authName": "订单",
        "path": "orders",
        "children": [{
            "id": 107,
            "authName": "订单管理",
            "path": "orders",
        }],
    },
    {
        "authName": "用户",
        "path": "users",
        "children": [{
            "authName": "用户管理",
            "path": "users",
        }],
    }
]