import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)
const originalReplace = VueRouter.prototype.replace;
VueRouter.prototype.replace = function replace(location) {
  return originalReplace.call(this, location).catch(err => err);
};
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: { name: 'First' },
    children: [
      {
        path: '/first',
        name: 'First',
        component: () => import(/* webpackChunkName: "first" */ '../views/index.vue')
      },
      {
        path: '/goods',
        name: 'Goods',
        component: () => import(/* webpackChunkName: "goods" */ '../views/goods/index.vue')
      },
      {
        path: '/category',
        name: 'Category',
        component: () => import(/* webpackChunkName: "category" */ '../views/categorys/index.vue')
      },
      {
        path: '/orders',
        name: 'Orders',
        component: () => import(/* webpackChunkName: "orders" */ '../views/orders/index.vue')
      },
      {
        path: '/users',
        name: 'Users',
        component: () => import(/* webpackChunkName: "users" */ '../views/users/index.vue')
      },
      {
        path: '/goods/details',
        name: 'Details',
        component: () => import(/* webpackChunkName: "details" */ '../views/goods/details.vue')
      },
      {
        path: '/goods/editgoods',
        name: 'EditGoods',
        component: () => import(/* webpackChunkName: "editgoods" */ '../views/goods/editGodds.vue')
      },
      {
        path: '/goods/addgood',
        name: 'AddGood',
        component: () => import(/* webpackChunkName: "addgood" */ '../views/goods/addGood.vue')
      },
      {
        path: '/category/lookcategorys',
        name: 'LookCategorys',
        component: () => import(/* webpackChunkName: "lookcategorys" */ '../views/categorys/LookCategorys.vue')
      },
      {
        path: '/orders/orderdetails',
        name: 'OrderDetails',
        component: () => import(/* webpackChunkName: "orderdetails" */ '../views/orders/OrderDetails.vue')
      },
      // {
      //   path: '/goods/addgoods',
      //   name: 'Addgoods',
      //   component: () => import(/* webpackChunkName: "addgoods" */ '../views/goods/addgoods.vue')
      // }

    ]
  },


]

const router = new VueRouter({
  routes
})

export default router
